package util;

import java.security.Key;
import java.util.Base64;

import javax.crypto.spec.SecretKeySpec;

import io.jsonwebtoken.SignatureAlgorithm;

public class SimpleKeyGenerator implements KeyGenerator {

	@Override
	public Key generateKey() {
		//O modo máis sinxelo de crear un token JWT asinado é usando un secreto HMAC 
		//HMAC (hash-based message authentication code) verifica 
		//a integridade de datos e a autenticidade dun token. 
		//nota: secret debería ser cargado dende env /configuration  
		// e ten que ter unha lonxitude suficiente
		String secret ="asdfSFS34wfsdfsdfSDSD32dfsddDDerQSNCK34SOWEK5354fdgdf4";
		Key hmackey = new SecretKeySpec(Base64.getDecoder().decode(secret), SignatureAlgorithm.HS256.getJcaName());			
		return hmackey;
	}

}
