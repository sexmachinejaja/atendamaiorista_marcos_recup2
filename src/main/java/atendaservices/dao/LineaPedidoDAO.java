package atendaservices.dao;

import java.sql.Connection;
import java.util.List;

import atendaservices.model.LineaPedido;
import atendaservices.model.Pedido;

public interface LineaPedidoDAO {

	 public int inserta (Connection connection, LineaPedido lineaPedido, Pedido pedido) throws Exception; // inserta LineaPedido e devolve id
	 public void actualiza (Connection connection, LineaPedido lineaPedido) throws Exception; // actualiza LineaPedido (unidades só)
	 public int getUnidadesDevoltasDe (Connection connection, LineaPedido lineaPedido) throws Exception;   // devolve as devolucions xa feitas dunha li�a pedido
	 /// novo en v. 230102
	 public void borra(Connection connection, LineaPedido linea) throws Exception;// borra linea pedido.
	
	 
	 public List<LineaPedido> getByPedido(Connection connection, Long idPedido) throws Exception;//devolve a lista de lineas dun pedido parametro
	 public LineaPedido getById(Connection connection, Long idLinea) throws Exception;
}
