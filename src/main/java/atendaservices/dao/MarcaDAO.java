package atendaservices.dao;

import java.sql.Connection;
import java.util.ArrayList;

import atendaservices.model.Marca;

public interface MarcaDAO {
	
//	public ArrayList<Marca> getAllMarcas() throws Exception; // devolve a lista das marcas
//	public void actualiza(Marca m) throws Exception; // actualiza marca
//	public int inserta(Marca m) throws Exception; /// inserta marca
//	public void borra(Marca m) throws Exception; // borra marca
//	public Marca getMarcaPorId(Long idMarca) throws Exception; // obten marca por id

	
		//Service
	public ArrayList<Marca> getAllMarcas(Connection connection) throws Exception;
	public int inserta(Connection connection, Marca m) throws Exception;
	public void borra(Connection connections, Marca m) throws Exception;
	public void actualiza(Connection connection, Marca m) throws Exception;
	public Marca getMarcaPorId(Connection connection, Long idMarca) throws Exception;
	
	
}
