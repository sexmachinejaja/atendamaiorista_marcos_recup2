package atendaservices.dao;

import java.sql.Connection;
import java.util.ArrayList;

import atendaservices.exceptions.DataException;
import atendaservices.model.Opinion;
import atendaservices.model.Produto;

public interface OpinionDAO {
	//modificado para capa service
//	public ArrayList<Opinion>  getOpinions(Produto produto) throws Exception ; // devolve as opinions sobre un produto 																							// como pares valoracion-numero de valoracions
	ArrayList<Opinion> getOpinions(Connection connection, Produto produto) throws DataException;
	//public int inserta(Opinion comentario) throws Exception; // insertar unha opinión na base de datos
	public int inserta(Connection connection, Opinion comentario) throws DataException; // insertar unha opinión na base de datos
	 
//	public int getValoracionMedia(Produto produto) throws Exception; // obten a valoración media enteira dun produto 
	int getValoracionMedia(Connection connection, Produto produto) throws DataException;
	
//	 public LinkedHashMap<Integer, Integer> getValoracions (Produto produto) throws Exception; // devolve as valoracións sobre un produto
	 
}
