package atendaservices.dao.util;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DAOUtil {

	public static int countRow(ResultSet resultSet) {
		int rows=0;
		try {
			resultSet.last();
			rows = resultSet.getRow();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return rows;
	}
}
