package atendaservices.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import atendaservices.dao.PedidoDAO;
import atendaservices.dao.UsuarioDAO;
import atendaservices.dao.LineaPedidoDAO;
import atendaservices.dao.ProdutoDAO;
import atendaservices.dao.Results;
import atendaservices.dao.util.ConnectionManager;
import atendaservices.dao.util.DAOUtil;
import atendaservices.exceptions.DataException;
import atendaservices.model.LineaPedido;
import atendaservices.model.Pedido;
import atendaservices.model.PedidoCriteria;
import atendaservices.model.Produto;
import atendaservices.model.Usuario;

public class PedidoDAOImpl implements PedidoDAO {

	private static final Logger logger = Logger.getAnonymousLogger();
	private UsuarioDAO usuarioDAO = new UsuarioDAOImpl();
	private LineaPedidoDAO lineaPedidoDAO = new LineaPedidoDAOImpl();
	private ProdutoDAO produtoDAO = new ProdutoDAOImpl();
	
	@Override
	public Pedido getPedidoPorId(Connection connection, int id) throws Exception {
		Pedido pedido = null;
		StringBuilder sql = new StringBuilder();
		ResultSet resultSet = null;
		PreparedStatement preparedStatement = null;
//		Connection connection=ConnectionManager.getConnection();
		Long idPedido=(long) id;
		try {
			sql.append("select p.id, p.id_pedido_devol, p.id_cliente, p.data_hora, p.pechado, p.recibido");
			sql.append(" from pedido as p ");
			sql.append("where p.id = ?");
			preparedStatement = connection.prepareStatement(sql.toString(), ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			int i = 1;
			preparedStatement.setLong(i++, idPedido);
			logger.info("create statement"+sql);
			resultSet = preparedStatement.executeQuery();
			if(resultSet.next()) {
				pedido=loadNext(connection, resultSet);
			}else {
				logger.info("resultSet vacio, sin resultados");
			}
			
		}catch(Exception e) {
			logger.log(Level.SEVERE, e.getMessage());
		}finally {
			ConnectionManager.closeResultSet(resultSet);
			ConnectionManager.closePreparedStatement(preparedStatement);
//			ConnectionManager.closeConnection(connection);
		}
		return pedido;
	}

	@Override
	public ArrayList<Pedido> getPedidosPeriodo(Connection connection, PedidoCriteria pedidoCriteria) throws Exception {
		ArrayList<Pedido> pedidos=new ArrayList<Pedido>();
		Boolean incluirMas=false;
		StringBuilder sql = new StringBuilder();
		ResultSet resultSet = null;
		PreparedStatement preparedStatement = null;
//		Connection connection=ConnectionManager.getConnection();
		try {
			sql.append("select p.id, p.id_pedido_devol, p.id_cliente, p.data_hora, p.pechado, p.recibido");
			sql.append(" from pedido as p where ");
			if(pedidoCriteria.getDende() != null) {
				sql.append(" p.data_hora >= ? ");
				incluirMas=true;
			}
			if(pedidoCriteria.getAta() != null) {
				if(incluirMas) {
					sql.append(" && ");
				}
				incluirMas=true;
				sql.append(" p.data_hora <= ? ");
			}
			if(pedidoCriteria.getConDevolucions() != null) {
				if(incluirMas) {
					sql.append(" && ");
				}
				incluirMas=true;
				if(pedidoCriteria.getConDevolucions()) {
					sql.append(" exists (select 1 from pedido where id_pedido_devol = p.id) ");
				}else {
					sql.append(" not exists (select 1 from pedido where id_pedido_devol = p.id) ");
				}
			}
			if(pedidoCriteria.getUsuario() != null) {
				if(incluirMas) {
					sql.append(" && ");
				}
				incluirMas=true;
				sql.append(" p.id_cliente = ? ");
			}
			if(pedidoCriteria.getPechado() != null) {
				if(incluirMas) {
					sql.append(" && ");
				}
				sql.append(" p.pechado = ? ");
			}
			if(pedidoCriteria.getRecibido() != null) {
				if(incluirMas) {
					sql.append(" && ");
				}
				sql.append(" p.recibido = ? ");
			}

			if(pedidoCriteria.getDende() == null && pedidoCriteria.getAta() == null && pedidoCriteria.getUsuario() == null 
					&& pedidoCriteria.getPechado() == null && pedidoCriteria.getRecibido() == null && pedidoCriteria.getConDevolucions() == null) {
				sql.append(" 1");
			}
			
			preparedStatement = connection.prepareStatement(sql.toString(), ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			
			int i=1;
			if(pedidoCriteria.getDende() != null) {
				preparedStatement.setObject(i++, pedidoCriteria.getDende());
			}
			if(pedidoCriteria.getAta() != null) {
				preparedStatement.setObject(i++, pedidoCriteria.getAta());
			}
			
			if(pedidoCriteria.getUsuario() != null) {
				preparedStatement.setLong(i++, pedidoCriteria.getUsuario().getId());
			}
			if(pedidoCriteria.getPechado() != null) {
				preparedStatement.setBoolean(i++, pedidoCriteria.getPechado());
			}
			if(pedidoCriteria.getRecibido() != null) {
				preparedStatement.setBoolean(i++, pedidoCriteria.getRecibido());
			}
			
			logger.info(preparedStatement.toString());
			resultSet = preparedStatement.executeQuery();
			
			while(resultSet.next()) {
				Pedido pedido = loadNext(connection, resultSet);
				pedidos.add(pedido);
			}
			return pedidos;
//			//total de resultados
//			int total=DAOUtil.countRow(resultSet);
//			
//			resultSet.absolute(startIndex);
//			while(resultSet.next() 
//					&& resultSet.getRow() < (startIndex + count) ) {
//				Produto produto=new Produto();
//				produto=loadNext(resultSet);
//				prodList.add(produto);
//			}
//			
//			return new Results<Produto>(prodList, startIndex, total);
			
		}catch(Exception e) {
			logger.log(Level.SEVERE, e.getMessage());
		}finally {
			ConnectionManager.closeResultSet(resultSet);
			ConnectionManager.closePreparedStatement(preparedStatement);
//			ConnectionManager.closeConnection(connection);
		}
		return null;
	}


	@Override
	public ArrayList<Pedido> getDevolucionsDe(Connection connection, Pedido pedido) throws Exception {
		ArrayList<Pedido> pedidos = new ArrayList();
		StringBuilder sql = new StringBuilder();
		ResultSet resultSet = null;
		PreparedStatement preparedStatement = null;
//		Connection connection=ConnectionManager.getConnection();
		try {
			sql.append("select p.id, p.id_pedido_devol, p.id_cliente, p.data_hora, p.pechado, p.recibido");
			sql.append(" from pedido as p ");
			sql.append(" where p.id_pedido_devol = ? ");
			preparedStatement = connection.prepareStatement(sql.toString(), ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			int i = 1;
			preparedStatement.setLong(i++, pedido.getId());
			logger.info("create statement"+sql);
			resultSet = preparedStatement.executeQuery();
			while(resultSet.next()) {
				Pedido ped=loadNext(connection, resultSet);
				pedidos.add(ped);
			}
		}catch(Exception e) {
			logger.log(Level.SEVERE, e.getMessage());
		}finally {
			ConnectionManager.closeResultSet(resultSet);
			ConnectionManager.closePreparedStatement(preparedStatement);
//			ConnectionManager.closeConnection(connection);
		}
		return pedidos;
	}

	@Override
	public int inserta(Connection connection, Pedido pedido) throws Exception {
		StringBuilder sql = new StringBuilder();
		ResultSet resultSet = null;
		PreparedStatement preparedStatement = null;
		Long lastId=0L;
//		Connection connection=ConnectionManager.getConnection();
		try {
			if(pedido.getLineasPedido()!=null) {
				for(LineaPedido linea:pedido.getLineasPedido()) {
					lineaPedidoDAO.inserta(connection, linea, pedido);
				}
			}
			if(pedido.getId_pedido_devol()!=null) {
				sql.append("insert into pedido "
						+ "(id_pedido_devol, id_cliente, data_hora, pechado, recibido)"+
						"values (?,?,?,?,?)");
				preparedStatement = connection.prepareStatement(sql.toString(), ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
				preparedStatement.setLong(1, pedido.getId_pedido_devol());
				preparedStatement.setLong(2, pedido.getCliente().getId());
				preparedStatement.setObject(3, pedido.getData());
				preparedStatement.setBoolean(4, pedido.getPechado());
				preparedStatement.setBoolean(5, pedido.getRecibido());
			}else {
				sql.append("insert into pedido "
						+ "(id_cliente, data_hora, pechado, recibido)"+
						"values (?,?,?,?)");
				preparedStatement = connection.prepareStatement(sql.toString(), ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
				preparedStatement.setLong(1, pedido.getCliente().getId());
				preparedStatement.setObject(2, pedido.getData());
				preparedStatement.setBoolean(3, pedido.getPechado());
				preparedStatement.setBoolean(4, pedido.getRecibido());
			}
			logger.info("create statement: "+sql);
			preparedStatement.execute();
			resultSet= connection.prepareStatement("SELECT LAST_INSERT_ID() as lastId").executeQuery();
			if(resultSet.next()) {
				lastId=resultSet.getLong("lastId");
			}
		}catch(Exception e) {
			logger.log(Level.SEVERE, e.getMessage());
		}finally {
			ConnectionManager.closeResultSet(resultSet);
			ConnectionManager.closePreparedStatement(preparedStatement);
//			ConnectionManager.closeConnection(connection);
		}
		return Integer.parseInt(lastId.toString());
	}

	@Override
	public void actualiza(Connection connection, Pedido pedido) throws Exception {
		StringBuilder sql = new StringBuilder();
		PreparedStatement preparedStatement = null;
		Boolean incluirMas=false;
//		Connection connection=ConnectionManager.getConnection();
		try {
			sql.append("update pedido set ");
			if(pedido.getPechado()!=null) {
				incluirMas=true;
				sql.append(" pechado = ?");
			}
			if(pedido.getRecibido()!=null) {
				if(incluirMas) {
					sql.append(", ");
				}
				sql.append(" recibido = ?");
			}
			
			sql.append(" where id = ? ");
			preparedStatement = connection.prepareStatement(sql.toString(), ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			
			int i=1;
			
			if(pedido.getPechado()!=null) {
				preparedStatement.setBoolean(i++,  pedido.getPechado());
			}
			if(pedido.getRecibido()!=null) {
				preparedStatement.setBoolean(i++,  pedido.getRecibido());
			}
			preparedStatement.setLong(i++, pedido.getId());
			logger.info("create statement: "+sql);
			preparedStatement.execute();
		}catch(Exception e) {
			logger.log(Level.SEVERE, e.getMessage());
		}
		finally {
			ConnectionManager.closePreparedStatement(preparedStatement);
//			ConnectionManager.closeConnection(connection);
		}
	}
	
	@Override
	public void borra (Connection connection, Pedido pedido) throws Exception{
		StringBuilder sql = new StringBuilder();
		PreparedStatement preparedStatement = null;
		Boolean incluirMas=false;
//		Connection connection=ConnectionManager.getConnection();
		try {
			sql.append(" delete from pedido where id = ? ");
			preparedStatement = connection.prepareStatement(sql.toString(), ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			
			int i=1;
			preparedStatement.setLong(i++,  pedido.getId());
			logger.info("create statement: "+sql);
			preparedStatement.execute();
		}catch(Exception e) {
			logger.log(Level.SEVERE, e.getMessage());
		}
		finally {
			ConnectionManager.closePreparedStatement(preparedStatement);
//			ConnectionManager.closeConnection(connection);
		}
	}
	
	private Pedido loadNext(Connection connection, ResultSet resultSet) {
		int i = 1;
		Pedido pedido=new Pedido();
		StringBuilder sql = new StringBuilder();
		ArrayList <LineaPedido> lineas=new ArrayList();
		try {
			pedido.setId(resultSet.getLong(i++));
			pedido.setId_pedido_devol(resultSet.getLong(i++));
			pedido.setCliente(usuarioDAO.findById(connection, resultSet.getLong(i++)));
			pedido.setData((LocalDateTime)(resultSet.getObject(i++)));
			pedido.setPechado(resultSet.getBoolean(i++));
			pedido.setRecibido(resultSet.getBoolean(i++));
			pedido.setLineasPedido(lineaPedidoDAO.getByPedido(connection, pedido.getId()));
			
		}catch (SQLException e) {
			e.printStackTrace();
			logger.info("error en loadNext de Pedido");
		}
		catch (Exception e) {
			e.printStackTrace();
			logger.info("error en loadNext de Pedido");
		}
		return pedido;
	}

}
