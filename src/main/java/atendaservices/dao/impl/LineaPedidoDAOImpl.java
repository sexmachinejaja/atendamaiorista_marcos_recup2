package atendaservices.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import atendaservices.dao.LineaPedidoDAO;
import atendaservices.dao.ProdutoDAO;
import atendaservices.dao.util.ConnectionManager;
import atendaservices.model.LineaPedido;
import atendaservices.model.Pedido;

public class LineaPedidoDAOImpl implements LineaPedidoDAO {

	private ProdutoDAO produtoDAO = new ProdutoDAOImpl();
	private static final Logger logger = Logger.getAnonymousLogger();
	
	@Override
	public int inserta(Connection connection, LineaPedido lineaPedido, Pedido pedido) throws Exception {
		if(!pedido.getPechado()) {
			StringBuilder sql = new StringBuilder();
			ResultSet resultSet = null;
			PreparedStatement preparedStatement = null;
			Long lastId=0L;
//			Connection connection=ConnectionManager.getConnection();
			try {
				sql.append("insert into linea_pedido (id_pedido,  id_produto, desconto, unidades, prezo, coste)"
						+ "values(?, ?, ?, ?, ?, ?)");
				preparedStatement = connection.prepareStatement(sql.toString(), ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
				preparedStatement.setLong(1, pedido.getId());
				preparedStatement.setLong(2, lineaPedido.getProduto().getId());
				preparedStatement.setInt(3, Integer.parseInt(lineaPedido.getDesconto().toString()));
				preparedStatement.setInt(4, Integer.parseInt(lineaPedido.getUnidades().toString()));
				preparedStatement.setDouble(5, lineaPedido.getPrezo());
				preparedStatement.setDouble(6, lineaPedido.getCoste());
				logger.info("create statement: "+sql);
				preparedStatement.execute();
				resultSet= connection.prepareStatement("SELECT LAST_INSERT_ID() as lastId").executeQuery();
				if(resultSet.next()) {
					lastId=resultSet.getLong("lastId");
				}
			}catch (SQLException e) {
				e.printStackTrace();
			}
			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally {
				ConnectionManager.closeResultSet(resultSet);
				ConnectionManager.closePreparedStatement(preparedStatement);
//				ConnectionManager.closeConnection(connection);
			}
			return Integer.parseInt(lastId.toString());
		}else {
			logger.info("Pedido pechado");
			return 0;
		}
	}

	@Override
	public void actualiza(Connection connection, LineaPedido lineaPedido) throws Exception {
		if(lineaPedido.getId()!=null) {
			if(lineaPedido.getUnidades()!=null) {
				StringBuilder sql = new StringBuilder();
				PreparedStatement preparedStatement = null;
//				Connection connection=ConnectionManager.getConnection();
				try {
					sql.append("update linea_pedido set unidades = ? where id = ?");
					
					preparedStatement = connection.prepareStatement(sql.toString(), ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
					
					int i=1;
					preparedStatement.setLong(i++, lineaPedido.getUnidades());
					preparedStatement.setLong(i++, lineaPedido.getId());
					logger.info("create statement: "+sql);
					preparedStatement.execute();
					
				}catch(Exception e) {
					logger.log(Level.SEVERE, e.getMessage());
				}
				finally {
					ConnectionManager.closePreparedStatement(preparedStatement);
//					ConnectionManager.closeConnection(connection);
				}
			}
		}
	}

	@Override
	public int getUnidadesDevoltasDe(Connection connection, LineaPedido lineaPedido) throws Exception {
//		StringBuilder sql = new StringBuilder();
//		PreparedStatement preparedStatement = null;
//		//Connection connection=ConnectionManager.getConnection();
//		try {
//			sql.append("select from pedido where id_pedido_devol = ?");
//			preparedStatement = connection.prepareStatement(sql.toString(), ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
//			int j = 1;
//			preparedStatement.setLong(j++, lineaPedido.get);
//			logger.info("create statement"+sql);
//			preparedStatement.executeQuery();
//		}catch (SQLException e) {
//			e.printStackTrace();
//		}
//		catch (Exception e) {
//			e.printStackTrace();
//		}finally {
//			ConnectionManager.closePreparedStatement(preparedStatement);
//			//ConnectionManager.closeConnection(connection);
//		}
		return 0;
	}

	@Override
	public void borra(Connection connection, LineaPedido linea) throws Exception {
		StringBuilder sql = new StringBuilder();
		PreparedStatement preparedStatement = null;
//		Connection connection=ConnectionManager.getConnection();
		try {
			sql.append("delete from linea_pedido where id = ?");
			preparedStatement = connection.prepareStatement(sql.toString(), ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			int j = 1;
			preparedStatement.setLong(j++, linea.getId());
			logger.info("create statement"+sql);
			preparedStatement.execute();
		}catch (SQLException e) {
			e.printStackTrace();
		}
		catch (Exception e) {
			e.printStackTrace();
		}finally {
			ConnectionManager.closePreparedStatement(preparedStatement);
//			ConnectionManager.closeConnection(connection);
		}
	}
	
	@Override
	public ArrayList<LineaPedido> getByPedido(Connection connection, Long idPedido) throws Exception  {
		ArrayList<LineaPedido> lineas=new ArrayList();
		StringBuilder sql = new StringBuilder();
		ResultSet resultSet = null;
		PreparedStatement preparedStatement = null;
		Long lastId=0L;
//		Connection connection=ConnectionManager.getConnection();
		try {
			sql.append("select lp.id, lp.id_pedido, lp.id_produto, lp.desconto, lp.unidades, lp.prezo, lp.coste");
			sql.append(" from linea_pedido as lp ");
			sql.append(" where lp.id_pedido = ? ");
			preparedStatement = connection.prepareStatement(sql.toString(), ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			int j = 1;
			preparedStatement.setLong(j++, idPedido);
			logger.info("create statement"+sql);
			resultSet = preparedStatement.executeQuery();
			while(resultSet.next()) {
				int i=1;
				LineaPedido linea=new LineaPedido();
				linea.setId(resultSet.getLong(i++));
				i++;
				linea.setProduto(produtoDAO.findById(connection, resultSet.getLong(i++)));
				linea.setDesconto(resultSet.getLong(i++));
				linea.setUnidades(resultSet.getLong(i++));
				linea.setPrezo(resultSet.getDouble(i++));
				linea.setCoste(resultSet.getDouble(i++));
				
				lineas.add(linea);
			}
		}catch (SQLException e) {
			e.printStackTrace();
		}
		catch (Exception e) {
			e.printStackTrace();
		}finally {
			ConnectionManager.closeResultSet(resultSet);
			ConnectionManager.closePreparedStatement(preparedStatement);
//			ConnectionManager.closeConnection(connection);
		}
		return lineas;
	}
	
	

	@Override
	public LineaPedido getById(Connection connection, Long idLinea) throws Exception  {
		LineaPedido linea=new LineaPedido();
		StringBuilder sql = new StringBuilder();
		ResultSet resultSet = null;
		PreparedStatement preparedStatement = null;
		Long lastId=0L;
//		Connection connection=ConnectionManager.getConnection();
		try {
			sql.append("select lp.id, lp.id_pedido, lp.id_produto, lp.desconto, lp.unidades, lp.prezo, lp.coste");
			sql.append(" from linea_pedido as lp ");
			sql.append(" where lp.id = ? ");
			preparedStatement = connection.prepareStatement(sql.toString(), ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			int j = 1;
			preparedStatement.setLong(j++, idLinea);
			logger.info("create statement"+sql);
			resultSet = preparedStatement.executeQuery();
			if(resultSet.next()) {
				int i=1;
				linea.setId(resultSet.getLong(i++));
				i++;
				linea.setProduto(produtoDAO.findById(connection, resultSet.getLong(i++)));
				linea.setDesconto(resultSet.getLong(i++));
				linea.setUnidades(resultSet.getLong(i++));
				linea.setPrezo(resultSet.getDouble(i++));
				linea.setCoste(resultSet.getDouble(i++));
			}
		}catch (SQLException e) {
			e.printStackTrace();
		}
		catch (Exception e) {
			e.printStackTrace();
		}finally {
			ConnectionManager.closeResultSet(resultSet);
			ConnectionManager.closePreparedStatement(preparedStatement);
//			ConnectionManager.closeConnection(connection);
		}
		return linea;
	}
}
