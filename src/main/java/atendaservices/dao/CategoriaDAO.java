package atendaservices.dao;

import java.sql.Connection;
import java.util.ArrayList;

import atendaservices.model.Categoria;

public interface CategoriaDAO {
//	public ArrayList<Categoria> getAllCategorias() throws Exception; // devolve a lista das categorias
//	public void actualiza(Categoria c) throws Exception; // acgtualiza categoria
//	public int inserta(Categoria c) throws Exception; /// inserta categoria
//	public void borra(Categoria c) throws Exception; // borra categoria
//	public Categoria getCategoriaPorId(Long idCategoria) throws Exception; // obten categoria por id

	ArrayList<Categoria> getAllCategorias(Connection connection) throws Exception;
	void actualiza(Connection connection, Categoria c) throws Exception;
	int inserta(Connection connection, Categoria c) throws Exception;
	void borra(Connection connection, Categoria c) throws Exception;
	Categoria getCategoriaPorId(Connection connection, int idCategoria) throws Exception;
}
