package atendaservices.model;

import java.time.LocalDateTime;

public class PedidoCriteria {
	private LocalDateTime dende;
	private LocalDateTime ata;
	private Boolean conDevolucions;
	private Usuario usuario;
	private Boolean pechado;
	private Boolean recibido;
	
	public PedidoCriteria() {
		
	}

	
	public PedidoCriteria(LocalDateTime dende, LocalDateTime ata, Boolean conDevolucions, Usuario usuario,
			Boolean pechado, Boolean recibido) {
		super();
		this.dende = dende;
		this.ata = ata;
		this.conDevolucions = conDevolucions;
		this.usuario = usuario;
		this.pechado = pechado;
		this.recibido = recibido;
	}


	public LocalDateTime getDende() {
		return dende;
	}

	public void setDende(LocalDateTime dende) {
		this.dende = dende;
	}

	public LocalDateTime getAta() {
		return ata;
	}

	public void setAta(LocalDateTime ata) {
		this.ata = ata;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Boolean getPechado() {
		return pechado;
	}

	public void setPechado(Boolean pechado) {
		this.pechado = pechado;
	}

	public Boolean getRecibido() {
		return recibido;
	}

	public void setRecibido(Boolean recibido) {
		this.recibido = recibido;
	}

	
	public Boolean getConDevolucions() {
		return conDevolucions;
	}

	public void setConDevolucions(Boolean conDevolucions) {
		this.conDevolucions = conDevolucions;
	}

	@Override
	public String toString() {
		return "PedidoCriteria [dende=" + dende + ", ata=" + ata + ", devolucion=" + conDevolucions + ", usuario=" + usuario
				+ ", pechado=" + pechado + ", recibido=" + recibido + "]";
	}

	
}
