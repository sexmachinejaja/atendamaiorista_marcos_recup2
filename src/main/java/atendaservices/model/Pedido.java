package atendaservices.model;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Pedido {

	private Long id;
	private Long id_pedido_devol;
	private Usuario cliente;
	private LocalDateTime data;
	private Boolean pechado;
	private Boolean recibido;
	private List<LineaPedido> LineasPedido=new ArrayList<LineaPedido>();
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getId_pedido_devol() {
		return id_pedido_devol;
	}
	public void setId_pedido_devol(Long id_pedido_devol) {
		this.id_pedido_devol = id_pedido_devol;
	}
	public Usuario getCliente() {
		return cliente;
	}
	public void setCliente(Usuario cliente) {
		this.cliente = cliente;
	}
	public LocalDateTime getData() {
		return data;
	}
	public void setData(LocalDateTime data) {
		this.data = data;
	}
	public Boolean getPechado() {
		return pechado;
	}
	public void setPechado(Boolean pechado) {
		this.pechado = pechado;
	}
	public Boolean getRecibido() {
		return recibido;
	}
	public void setRecibido(Boolean recibido) {
		this.recibido = recibido;
	}
	public List<LineaPedido> getLineasPedido() {
		return LineasPedido;
	}
	public void setLineasPedido(List<LineaPedido> lineasPedido) {
		LineasPedido = lineasPedido;
	}
	@Override
	public int hashCode() {
		return Objects.hash(LineasPedido, cliente, data, id, id_pedido_devol, pechado, recibido);
	}
	@Override
	public String toString() {
		return "Pedido [id=" + id + ", id_pedido_devol=" + id_pedido_devol + ", cliente=" + cliente + ", data=" + data
				+ ", pechado=" + pechado + ", recibido=" + recibido + ", LineasPedido=" + LineasPedido + "]";
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pedido other = (Pedido) obj;
		return Objects.equals(LineasPedido, other.LineasPedido) && Objects.equals(cliente, other.cliente)
				&& Objects.equals(data, other.data) && Objects.equals(id, other.id)
				&& Objects.equals(id_pedido_devol, other.id_pedido_devol) && Objects.equals(pechado, other.pechado)
				&& Objects.equals(recibido, other.recibido);
	}
	
	
}
