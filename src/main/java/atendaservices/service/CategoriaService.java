package atendaservices.service;

import java.util.ArrayList;

import atendaservices.model.Categoria;

public interface CategoriaService {
	//copy from CategoriaDAO
	public ArrayList<Categoria> getAllCategorias() throws Exception; // devolve a lista das categorias
	public void actualiza(Categoria c) throws Exception; // actualiza categoria
	public int inserta(Categoria c) throws Exception; /// inserta categoria
	public void borra(Categoria c) throws Exception; // borra categoria
	public Categoria getCategoriaPorId(int idCategoria) throws Exception; // obten categoria por id
}
