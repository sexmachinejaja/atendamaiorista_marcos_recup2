package atendaservices.service;

import java.util.ArrayList;

import atendaservices.model.Opinion;
import atendaservices.model.Produto;

public interface OpinionService {
	//copy from OpinionDAO
	public ArrayList<Opinion>  getOpinions(Produto produto) throws Exception ; // devolve as opinions sobre un produto 																							// como pares valoracion-numero de valoracions
	public int inserta(Opinion comentario) throws Exception; // insertar unha opinión
	 
	public int getValoracionMedia(Produto produto) throws Exception; // obten a valoración media enteira dun produto 
}
