package atendaservices.service;

import java.sql.Connection;
import java.util.List;

import atendaservices.exceptions.DataException;
import atendaservices.model.LineaPedido;
import atendaservices.model.Pedido;

public interface LineaPedidoService {

	public int inserta (LineaPedido lineaPedido, Pedido pedido) throws DataException; // inserta LineaPedido e devolve id
	 public void actualiza (LineaPedido lineaPedido) throws DataException; // actualiza LineaPedido (unidades só)
	 public int getUnidadesDevoltasDe (LineaPedido lineaPedido) throws DataException;   // devolve as devolucions xa feitas dunha li�a pedido
	 /// novo en v. 230102
	 public void borra(LineaPedido linea) throws DataException;// borra linea pedido.
	
	 
	 public List<LineaPedido> getByPedido(Long idPedido) throws DataException;//devolve a lista de lineas dun pedido parametro
	 public LineaPedido getById(Long idLinea) throws DataException;
}
