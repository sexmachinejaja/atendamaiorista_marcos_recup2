package atendaservices.service.impl;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import atendaservices.dao.PedidoDAO;
import atendaservices.dao.impl.PedidoDAOImpl;
import atendaservices.dao.util.ConnectionManager;
import atendaservices.exceptions.DataException;
import atendaservices.model.Pedido;
import atendaservices.model.PedidoCriteria;
import atendaservices.model.Produto;
import atendaservices.service.PedidoService;

public class PedidoServiceImpl implements PedidoService {
	private static final Logger logger = Logger.getAnonymousLogger();
	private PedidoDAO pedidoDAO=new PedidoDAOImpl();
	
	@Override
	public Pedido getPedidoPorId(int id) throws DataException {
		Connection connection =ConnectionManager.getConnection();
		Pedido p=null;
		boolean commit=false;
		try {
			connection.setAutoCommit(false);
			p=pedidoDAO.getPedidoPorId(connection, id);
			commit=true;
			logger.info("Funcionamiento correcto. commit= "+commit);
		}catch(Exception e) {
			logger.log(Level.SEVERE, e.getMessage());
		}finally {
			ConnectionManager.closeConnection(connection, commit);
		}
		return p;
	}
	@Override
	public ArrayList<Pedido> getPedidosPeriodo(PedidoCriteria pedidoCriteria) throws DataException {
		Connection connection =ConnectionManager.getConnection();
		ArrayList<Pedido> pedidos = null;
		boolean commit=false;
		try {
			connection.setAutoCommit(false);
			pedidos=pedidoDAO.getPedidosPeriodo(connection, pedidoCriteria);
			commit=true;
			logger.info("Funcionamiento correcto. commit= "+commit);
		}catch(Exception e) {
			logger.log(Level.SEVERE, e.getMessage());
		}finally {
			ConnectionManager.closeConnection(connection, commit);
		}
		return pedidos;
	}
	@Override
	public ArrayList<Pedido> getDevolucionsDe(Pedido pedido) throws DataException {
		Connection connection =ConnectionManager.getConnection();
		ArrayList<Pedido> pedidos = null;
		boolean commit=false;
		try {
			connection.setAutoCommit(false);
			pedidos=pedidoDAO.getDevolucionsDe(connection, pedido);
			commit=true;
			logger.info("Funcionamiento correcto. commit= "+commit);
		}catch(Exception e) {
			logger.log(Level.SEVERE, e.getMessage());
		}finally {
			ConnectionManager.closeConnection(connection, commit);
		}
		return pedidos;
	}
	@Override
	public int inserta(Pedido pedido) throws DataException {
		Connection connection =ConnectionManager.getConnection();
		boolean commit=false;
		int idCreado=0;
		try {
			connection.setAutoCommit(false);
			idCreado=pedidoDAO.inserta(connection, pedido);
			commit=true;
			logger.info("Funcionamiento correcto. commit= "+commit);
		}catch(Exception e) {
			logger.log(Level.SEVERE, e.getMessage());
		}finally {
			ConnectionManager.closeConnection(connection, commit);
		}
		return idCreado;
	}
	@Override
	public void actualiza(Pedido pedido) throws DataException {
		Connection connection =ConnectionManager.getConnection();
		boolean commit=false;
		try {
			connection.setAutoCommit(false);
			pedidoDAO.actualiza(connection, pedido);
			commit=true;
			logger.info("Funcionamiento correcto. commit= "+commit);
			
		}catch(Exception e) {
			logger.log(Level.SEVERE, e.getMessage());
		}finally {
			ConnectionManager.closeConnection(connection, commit);
		}
	}
	@Override
	public void borra(Pedido pedido) throws DataException {
		Connection connection =ConnectionManager.getConnection();
		boolean commit=false;
		try {
			connection.setAutoCommit(false);
			pedidoDAO.borra(connection, pedido);
			commit=true;
			logger.info("Funcionamiento correcto. commit= "+commit);
			
		}catch(Exception e) {
			logger.log(Level.SEVERE, e.getMessage());
		}finally {
			ConnectionManager.closeConnection(connection, commit);
		}
	}
	
}
