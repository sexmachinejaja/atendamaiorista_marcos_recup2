package atendaservices.service.impl;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import atendaservices.dao.CategoriaDAO;
import atendaservices.dao.impl.CategoriaDAOImpl;
import atendaservices.dao.util.ConnectionManager;
import atendaservices.model.Categoria;
import atendaservices.service.CategoriaService;
import atendaservices.service.ProdutoService;

public class CategoriaServiceImpl  implements CategoriaService{

	private static final Logger logger = Logger.getAnonymousLogger();
	private CategoriaDAO categoriaDAO=new CategoriaDAOImpl();
	
	@Override
	public ArrayList<Categoria> getAllCategorias() throws Exception {
		Connection connection =ConnectionManager.getConnection();
		ArrayList<Categoria> categorias=null;
		boolean commit=false;
		try {
			connection.setAutoCommit(false);
			categorias=categoriaDAO.getAllCategorias(connection);
			commit=true;
			logger.info("Funcionamiento correcto. commit= "+commit);
		}catch(Exception e) {
			logger.log(Level.SEVERE, e.getMessage());
		}finally {
			ConnectionManager.closeConnection(connection, commit);
		}
		return categorias;
	}

	@Override
	public void actualiza(Categoria c) throws Exception {
		Connection connection =ConnectionManager.getConnection();
		boolean commit=false;
		try {
			connection.setAutoCommit(false);
			categoriaDAO.actualiza(connection, c);
			commit=true;
			logger.info("Funcionamiento correcto. commit= "+commit);
		}catch(Exception e) {
			logger.log(Level.SEVERE, e.getMessage());
		}finally {
			ConnectionManager.closeConnection(connection, commit);
		}
	}

	@Override
	public int inserta(Categoria c) throws Exception {
		Connection connection =ConnectionManager.getConnection();
		boolean commit=false;
		int idCreado=0;
		try {
			connection.setAutoCommit(false);
			idCreado=categoriaDAO.inserta(connection, c);
			commit=true;
			logger.info("Funcionamiento correcto. commit= "+commit);
		}catch(Exception e) {
			logger.log(Level.SEVERE, e.getMessage());
		}finally {
			ConnectionManager.closeConnection(connection, commit);
		}
		return idCreado;
	}

	@Override
	public void borra(Categoria c) throws Exception {
		Connection connection =ConnectionManager.getConnection();
		boolean commit=false;
		try {
			connection.setAutoCommit(false);
			categoriaDAO.borra(connection, c);
			commit=true;
			logger.info("Funcionamiento correcto. commit= "+commit);
		}catch(Exception e) {
			logger.log(Level.SEVERE, e.getMessage());
		}finally {
			ConnectionManager.closeConnection(connection, commit);
		}
	}

	@Override
	public Categoria getCategoriaPorId(int idCategoria) throws Exception {
		Connection connection =ConnectionManager.getConnection();
		Categoria c=null;
		boolean commit=false;
		try {
			connection.setAutoCommit(false);
			c = categoriaDAO.getCategoriaPorId(connection, idCategoria);
			commit=true;
			logger.info("Funcionamiento correcto. commit= "+commit);
		}catch(Exception e) {
			logger.log(Level.SEVERE, e.getMessage());
		}finally {
			ConnectionManager.closeConnection(connection, commit);
		}
		return c;
	}

}
