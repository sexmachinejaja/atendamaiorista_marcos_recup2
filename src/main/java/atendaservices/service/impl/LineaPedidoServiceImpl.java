package atendaservices.service.impl;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import atendaservices.dao.LineaPedidoDAO;
import atendaservices.dao.impl.LineaPedidoDAOImpl;
import atendaservices.dao.util.ConnectionManager;
import atendaservices.exceptions.DataException;
import atendaservices.model.LineaPedido;
import atendaservices.model.Pedido;
import atendaservices.model.Produto;
import atendaservices.service.LineaPedidoService;

public class LineaPedidoServiceImpl implements LineaPedidoService{

	private static final Logger logger = Logger.getAnonymousLogger();
	private LineaPedidoDAO lineaPedidoDAO=new LineaPedidoDAOImpl();
	
	@Override
	public int inserta(LineaPedido lineaPedido, Pedido pedido) throws DataException {
		Connection connection =ConnectionManager.getConnection();
		boolean commit=false;
		int idCreado=0;
		try {
			connection.setAutoCommit(false);
			idCreado=lineaPedidoDAO.inserta(connection, lineaPedido, pedido);
			commit=true;
			logger.info("Funcionamiento correcto. commit= "+commit);
		}catch(Exception e) {
			logger.log(Level.SEVERE, e.getMessage());
		}finally {
			ConnectionManager.closeConnection(connection, commit);
		}
		return idCreado;
	}

	@Override
	public void actualiza(LineaPedido lineaPedido) throws DataException {
		Connection connection =ConnectionManager.getConnection();
		boolean commit=false;
		try {
			connection.setAutoCommit(false);
			lineaPedidoDAO.actualiza(connection, lineaPedido);
			commit=true;
			logger.info("Funcionamiento correcto. commit= "+commit);
			
		}catch(Exception e) {
			logger.log(Level.SEVERE, e.getMessage());
		}finally {
			ConnectionManager.closeConnection(connection, commit);
		}
	}

	@Override
	public int getUnidadesDevoltasDe(LineaPedido lineaPedido) throws DataException {
		Connection connection =ConnectionManager.getConnection();
		boolean commit=false;
		int idCreado=0;
		try {
			connection.setAutoCommit(false);
			idCreado=lineaPedidoDAO.getUnidadesDevoltasDe(connection, lineaPedido);
			commit=true;
			logger.info("Funcionamiento correcto. commit= "+commit);
			
		}catch(Exception e) {
			logger.log(Level.SEVERE, e.getMessage());
		}finally {
			ConnectionManager.closeConnection(connection, commit);
		}
		return idCreado;
	}

	@Override
	public void borra(LineaPedido linea) throws DataException {
		Connection connection =ConnectionManager.getConnection();
		boolean commit=false;
		try {
			connection.setAutoCommit(false);
			lineaPedidoDAO.borra(connection, linea);
			commit=true;
			logger.info("Funcionamiento correcto. commit= "+commit);
			
		}catch(Exception e) {
			logger.log(Level.SEVERE, e.getMessage());
		}finally {
			ConnectionManager.closeConnection(connection, commit);
		}
	}

	@Override
	public List<LineaPedido> getByPedido(Long idPedido) throws DataException {
		Connection connection =ConnectionManager.getConnection();
		ArrayList<LineaPedido> lineas=null;
		boolean commit=false;
		try {
			connection.setAutoCommit(false);
			lineas=(ArrayList<LineaPedido>) lineaPedidoDAO.getByPedido(connection, idPedido);
			commit=true;
			logger.info("Funcionamiento correcto. commit= "+commit);
		}catch(Exception e) {
			logger.log(Level.SEVERE, e.getMessage());
		}finally {
			ConnectionManager.closeConnection(connection, commit);
		}
		return lineas;
	}

	@Override
	public LineaPedido getById(Long idLinea) throws DataException {
		Connection connection =ConnectionManager.getConnection();
		LineaPedido lp=null;
		boolean commit=false;
		try {
			connection.setAutoCommit(false);
			lp=lineaPedidoDAO.getById(connection, idLinea);
			commit=true;
			logger.info("Funcionamiento correcto. commit= "+commit);
		}catch(Exception e) {
			logger.log(Level.SEVERE, e.getMessage());
		}finally {
			ConnectionManager.closeConnection(connection, commit);
		}
		return lp;
	}

}
