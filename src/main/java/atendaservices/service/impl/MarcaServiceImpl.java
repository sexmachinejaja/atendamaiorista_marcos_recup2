package atendaservices.service.impl;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import atendaservices.dao.MarcaDAO;
import atendaservices.dao.impl.MarcaDAOImpl;
import atendaservices.dao.util.ConnectionManager;
import atendaservices.model.Marca;
import atendaservices.model.Produto;
import atendaservices.service.MarcaService;
import atendaservices.service.ProdutoService;

public class MarcaServiceImpl  implements MarcaService{

	private static final Logger logger = Logger.getAnonymousLogger();
	private MarcaDAO marcaDAO=new MarcaDAOImpl();
	
	@Override
	public ArrayList<Marca> getAllMarcas() throws Exception {
		Connection connection =ConnectionManager.getConnection();
		ArrayList<Marca> marcas=null;
		boolean commit=false;
		try {
			connection.setAutoCommit(false);
			marcas=marcaDAO.getAllMarcas(connection);
			commit=true;
			logger.info("Funcionamiento correcto. commit= "+commit);
		}catch(Exception e) {
			logger.log(Level.SEVERE, e.getMessage());
		}finally {
			ConnectionManager.closeConnection(connection, commit);
		}
		return marcas;
	}

	@Override
	public void actualiza(Marca m) throws Exception {
		Connection connection =ConnectionManager.getConnection();
		boolean commit=false;
		try {
			connection.setAutoCommit(false);
			marcaDAO.actualiza(connection, m);
			commit=true;
			logger.info("Funcionamiento correcto. commit= "+commit);
		}catch(Exception e) {
			logger.log(Level.SEVERE, e.getMessage());
		}finally {
			ConnectionManager.closeConnection(connection, commit);
		}
	}

	@Override
	public int inserta(Marca m) throws Exception {
		Connection connection =ConnectionManager.getConnection();
		boolean commit=false;
		int idCreado=0;
		try {
			connection.setAutoCommit(false);
			idCreado=marcaDAO.inserta(connection, m);
			commit=true;
			logger.info("Funcionamiento correcto. commit= "+commit);
		}catch(Exception e) {
			logger.log(Level.SEVERE, e.getMessage());
		}finally {
			ConnectionManager.closeConnection(connection, commit);
		}
		return idCreado;
	}

	@Override
	public void borra(Marca m) throws Exception {
		Connection connection =ConnectionManager.getConnection();
		boolean commit=false;
		try {
			connection.setAutoCommit(false);
			marcaDAO.borra(connection, m);
			commit=true;
			logger.info("Funcionamiento correcto. commit= "+commit);
		}catch(Exception e) {
			logger.log(Level.SEVERE, e.getMessage());
		}finally {
			ConnectionManager.closeConnection(connection, commit);
		}
	}

	@Override
	public Marca getMarcaPorId(Long idMarca) throws Exception {
		Connection connection =ConnectionManager.getConnection();
		Marca m=null;
		boolean commit=false;
		try {
			connection.setAutoCommit(false);
			m = marcaDAO.getMarcaPorId(connection, idMarca);
			commit=true;
			logger.info("Funcionamiento correcto. commit= "+commit);
		}catch(Exception e) {
			logger.log(Level.SEVERE, e.getMessage());
		}finally {
			ConnectionManager.closeConnection(connection, commit);
		}
		return m;
	}

}
