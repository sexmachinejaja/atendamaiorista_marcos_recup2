package atendaservices.service.impl;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import atendaservices.dao.OpinionDAO;
import atendaservices.dao.impl.OpinionDAOImpl;
import atendaservices.dao.util.ConnectionManager;
import atendaservices.exceptions.DataException;
import atendaservices.model.Opinion;
import atendaservices.model.Produto;
import atendaservices.service.OpinionService;
import atendaservices.service.ProdutoService;

public class OpinionServiceImpl  implements OpinionService{
	
	private static final Logger logger = Logger.getAnonymousLogger();
	private OpinionDAO opinionDAO=new OpinionDAOImpl();
	
	@Override
	public ArrayList<Opinion> getOpinions(Produto produto) throws DataException {
		Connection connection =ConnectionManager.getConnection();
		ArrayList<Opinion> opiniones=null;
		boolean commit=false;
		try {
			connection.setAutoCommit(false);
			opiniones=opinionDAO.getOpinions(connection, produto);
			commit=true;
			logger.info("Funcionamiento correcto. commit= "+commit);
		}catch (Exception e) {
			logger.log(Level.SEVERE, e.getMessage());
		}
		return opiniones;
	}

	@Override
	public int inserta(Opinion comentario) throws Exception {
		Connection connection =ConnectionManager.getConnection();
		boolean commit=false;
		int idCreado = 0;
		try {
			connection.setAutoCommit(false);
			idCreado=opinionDAO.inserta(connection, comentario);
			commit=true;
			logger.info("Funcionamiento correcto. commit= "+commit);
		}catch(Exception e) {
			logger.log(Level.SEVERE, e.getMessage());
		}finally {
			ConnectionManager.closeConnection(connection, commit);
		}
		return idCreado;
	}

	@Override
	public int getValoracionMedia(Produto produto) throws Exception {
		Connection connection =ConnectionManager.getConnection();
		boolean commit=false;
		int valoracionMedia = 0;
		try {
			connection.setAutoCommit(false);
			valoracionMedia=opinionDAO.getValoracionMedia(connection, produto);
			commit=true;
			logger.info("Funcionamiento correcto. commit= "+commit);
		}catch(Exception e) {
			logger.log(Level.SEVERE, e.getMessage());
		}finally {
			ConnectionManager.closeConnection(connection, commit);
		}
		return valoracionMedia;
	}

}
