package atendaservices.service.impl;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import atendaservices.dao.ProdutoDAO;
import atendaservices.dao.Results;
import atendaservices.dao.impl.ProdutoDAOImpl;
import atendaservices.dao.util.ConnectionManager;
import atendaservices.exceptions.DataException;
import atendaservices.model.Produto;
import atendaservices.model.ProdutoCriteria;
import atendaservices.service.ProdutoService;

public class ProdutoServiceImpl implements ProdutoService{

	private static final Logger logger = Logger.getAnonymousLogger();
	private ProdutoDAO produtoDAO=new ProdutoDAOImpl();
	
	@Override
	public Produto findById(Long idProduto) throws DataException {
		Connection connection =ConnectionManager.getConnection();
		Produto p=null;
		boolean commit=false;
		try {
			connection.setAutoCommit(false);
			p=produtoDAO.findById(connection, idProduto);
			commit=true;
			logger.info("Funcionamiento correcto. commit= "+commit);
		}catch(Exception e) {
			logger.log(Level.SEVERE, e.getMessage());
		}finally {
			ConnectionManager.closeConnection(connection, commit);
		}
		return p;
	}

	@Override
	public ArrayList<Produto> findAll() throws DataException {
		Connection connection =ConnectionManager.getConnection();
		ArrayList<Produto> produtos=null;
		boolean commit=false;
		try {
			connection.setAutoCommit(false);
			produtos=produtoDAO.findAll(connection);
			commit=true;
			logger.info("Funcionamiento correcto. commit= "+commit);
		}catch(Exception e) {
			logger.log(Level.SEVERE, e.getMessage());
		}finally {
			ConnectionManager.closeConnection(connection, commit);
		}
		return produtos;
	}

	@Override
	public Results<Produto> findBy(ProdutoCriteria produtoCriteria, int startIndex, int count) throws DataException {
		Connection connection =ConnectionManager.getConnection();
		boolean commit=false;
		Results<Produto> produtos=null;
		try {
			connection.setAutoCommit(false);
			produtos=produtoDAO.findBy(connection, produtoCriteria, startIndex, count);
			commit=true;
			logger.info("Funcionamiento correcto. commit= "+commit);
		}catch(Exception e) {
			logger.log(Level.SEVERE, e.getMessage());
		}finally {
			ConnectionManager.closeConnection(connection, commit);
		}
		return produtos;
	}

	@Override
	public Long create(Produto produto) throws DataException {
		Connection connection =ConnectionManager.getConnection();
		boolean commit=false;
		Long idCreado=null;
		try {
			connection.setAutoCommit(false);
			idCreado=produtoDAO.create(connection, produto);
			commit=true;
			logger.info("Funcionamiento correcto. commit= "+commit);
		}catch(Exception e) {
			logger.log(Level.SEVERE, e.getMessage());
		}finally {
			ConnectionManager.closeConnection(connection, commit);
		}
		return idCreado;
	}

	@Override
	public Produto update(Produto produto) throws DataException {
		Connection connection =ConnectionManager.getConnection();
		boolean commit=false;
		Produto produtoUpdated=null;
		try {
			connection.setAutoCommit(false);
			produtoUpdated=produtoDAO.update(connection, produto);
			commit=true;
			logger.info("Funcionamiento correcto. commit= "+commit);
			
		}catch(Exception e) {
			logger.log(Level.SEVERE, e.getMessage());
		}finally {
			ConnectionManager.closeConnection(connection, commit);
		}
		return produtoUpdated;
	}

	@Override
	public boolean softDelete(Long idProduto) throws DataException {
		Connection connection =ConnectionManager.getConnection();
		boolean commit=false;
		boolean borrado=false;
		try {
			connection.setAutoCommit(false);
			borrado= produtoDAO.softDelete(connection, idProduto);
			commit=true;
			logger.info("Funcionamiento correcto. commit= "+commit);
			
		}catch(Exception e) {
			logger.log(Level.SEVERE, e.getMessage());
		}finally {
			ConnectionManager.closeConnection(connection, commit);
		}
		return borrado;
	}

	@Override
	public boolean asignarProdutoCategoria(Long idProduto, Integer idCategoria) throws DataException {
		Connection connection =ConnectionManager.getConnection();
		boolean commit=false;
		try {
			connection.setAutoCommit(false);
			Produto produto=new Produto();
			produto.setId(idProduto);
			produto.setIdCategoria(idCategoria);
			produtoDAO.update(connection, produto);
			commit=true;
			logger.info("Funcionamiento correcto. commit= "+commit);
		}catch(Exception e) {
			logger.log(Level.SEVERE, e.getMessage());
		}finally {
			ConnectionManager.closeConnection(connection, commit);
		}
		return commit;
		
	}

	@Override
	public boolean asignarProdutoMarca(Long idProduto, Long idMarca) throws DataException {
		Connection connection =ConnectionManager.getConnection();
		boolean commit=false;
		try {
			connection.setAutoCommit(false);
			Produto produto=new Produto();
			produto.setId(idProduto);
			produto.setIdMarca(idMarca);
			produtoDAO.update(connection, produto);
			commit=true;
			logger.info("Funcionamiento correcto. commit= "+commit);
		}catch(Exception e) {
			logger.log(Level.SEVERE, e.getMessage());
		}finally {
			ConnectionManager.closeConnection(connection, commit);
		}
		return commit;
	}

}
