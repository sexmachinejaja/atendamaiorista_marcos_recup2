package atendaservices.service;

import java.sql.Connection;
import java.util.ArrayList;

import atendaservices.exceptions.DataException;
import atendaservices.model.Pedido;
import atendaservices.model.PedidoCriteria;

public interface PedidoService {

	 public Pedido getPedidoPorId(int id) throws DataException; // devolve pedido de campo id= id
	 // devolve os pedidos, abertos ou pechados entre as duas datas, cun usuario especificado ou non, con ou sin devolucions, e recibidos ou non
	 public ArrayList<Pedido> getPedidosPeriodo(PedidoCriteria pedidoCriteria) throws DataException;
	 public ArrayList<Pedido> getDevolucionsDe (Pedido pedido)throws DataException; // devolve a lista de pedidos devolucions do pedido parametro
	 public int inserta(Pedido pedido)throws DataException; // // inserta pedido como aberto, insertando tam�n lineas pedido que cont�n
	 public void actualiza(Pedido pedido)throws DataException; // actualiza todo menos lineas pedido
	 public void borra (Pedido pedido) throws DataException;// borra pedido
}
