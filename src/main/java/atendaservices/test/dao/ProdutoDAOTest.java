package atendaservices.test.dao;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import atendaservices.dao.ProdutoDAO;
import atendaservices.dao.Results;
import atendaservices.dao.impl.ProdutoDAOImpl;
import atendaservices.dao.util.ConnectionManager;
import atendaservices.model.Produto;
import atendaservices.service.ProdutoCriteria;

public class ProdutoDAOTest {
private static final Logger logger = Logger.getAnonymousLogger();
	
	
	public static void main(String[] args) {
		ProdutoDAO produtoDAO = new ProdutoDAOImpl(); 
		
		
		try {
//			logger.info("Metodo FindById");
//			logger.info((produtoDAO.findById(ConnectionManager.getConnection(), 1L)).toString());
//			
//			logger.info("Metodo FindAll");
//			ArrayList<Produto> listaTodosProdutos=produtoDAO.findAll(ConnectionManager.getConnection());
//			for(Produto produto:listaTodosProdutos) {
//				logger.info(produto.toString());
//			}
//			
//			logger.info("Metodo FindBy");
//			ProdutoCriteria criterio=new ProdutoCriteria(null, null, 1, null, null);
//			Results<Produto> listaProdutos=produtoDAO.findBy(ConnectionManager.getConnection(), criterio, 0, 1);
//			logger.info(listaProdutos.getPage().toString());
//			
//			logger.info("Metodo Create");
//			Produto p1=new Produto();
//			p1.setIdCategoria(1);
//			p1.setIdMarca(1L);
//			p1.setNome("prodProba");
//			p1.setPrezo(21.35);
//			p1.setDesconto(5);
//			p1.setCoste(20.05);
//			p1.setIva(3);
//			p1.setStock(30L);
//			p1.setFoto("probaProdutoIMAGEN");
//			p1.setBaixa(false);
//			produtoDAO.create(ConnectionManager.getConnection(), p1);
			
			logger.info("Metodo Update");
			Produto p2=new Produto();
			p2.setId(38L);
			p2.setIdCategoria(1);
			p2.setIdMarca(1L);
			p2.setNome("prodProbaNDO");
			p2.setPrezo(21.35);
			p2.setDesconto(5);
			p2.setCoste(20.05);
			p2.setIva(3);
			p2.setStock(30L);
			p2.setFoto("probaProdutoIMAGENprobando");
			p2.setBaixa(false);
			produtoDAO.update(ConnectionManager.getConnection(), p2);
			
			logger.info("Metodo SoftDelete");
			produtoDAO.softDelete(ConnectionManager.getConnection(), 33L);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
}