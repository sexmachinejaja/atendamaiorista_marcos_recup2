package atendaservices.test.dao;

import java.util.logging.Logger;

import atendaservices.dao.CategoriaDAO;
import atendaservices.dao.impl.CategoriaDAOImpl;
import atendaservices.model.Categoria;

public class CategoriaDAOTest {

	private static final Logger logger = Logger.getAnonymousLogger();
	
	public static void main(String[] args) {
		CategoriaDAO categoriaDAO=new CategoriaDAOImpl();
		
		try {
			logger.info("metodo getAllCategorias");
			logger.info(categoriaDAO.getAllCategorias().toString());
			
			logger.info("metodo getCategoriaPorId");
			logger.info(categoriaDAO.getCategoriaPorId(2L).toString());
			
//			logger.info("metodo inserta");
//			Categoria c1=new Categoria();
//			c1.setNome("categoriadeproba");
//			categoriaDAO.inserta(c1);
//			
//			logger.info("metodo actualiza");
//			Categoria c2=new Categoria();
//			c2.setId(4L);
//			c2.setNome("categoriadeprobamodificada");
//			categoriaDAO.actualiza(c2);
//			
			logger.info("metodo borra");
			Categoria c3=new Categoria();
			c3.setId(4L);
			categoriaDAO.borra(c3);
			
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

}
