package atendaservices.test.dao;

import java.util.logging.Logger;
import atendaservices.dao.MarcaDAO;
import atendaservices.dao.impl.MarcaDAOImpl;
import atendaservices.model.Marca;


public class MarcaDAOTest {

	private static final Logger logger = Logger.getAnonymousLogger();
	
	public static void main(String[] args) {
		MarcaDAO marcaDAO=new MarcaDAOImpl();
		
		try {
			logger.info("metodo getAllMarcas");
			logger.info(marcaDAO.getAllMarcas().toString());
			
			logger.info("metodo getMarcaPorId");
			logger.info(marcaDAO.getMarcaPorId(2L).toString());
//			
//			logger.info("metodo inserta");
//			Marca m1=new Marca();
//			m1.setNome("marcadeproba222");
//			marcaDAO.inserta(m1);
//			
//			logger.info("metodo actualiza");
//			Marca m2=new Marca();
//			m2.setId(4L);
//			m2.setNome("marcadeprobamodificada");
//			marcaDAO.actualiza(m2);
//			
//			logger.info("metodo borra");
//			Marca m3=new Marca();
//			m3.setId(5L);
//			marcaDAO.borra(m3);
			
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

}
