package atendaservices.test.dao;

import java.time.LocalDateTime;
import java.util.List;
import java.util.logging.Logger;
import atendaservices.dao.OpinionDAO;
import atendaservices.dao.impl.OpinionDAOImpl;
import atendaservices.model.Opinion;
import atendaservices.model.Produto;

public class OpinionDAOTest {

	private static final Logger logger = Logger.getAnonymousLogger();
	
	public static void main(String[] args) {
		OpinionDAO opinionDAO=new OpinionDAOImpl();
		
		try {
			logger.info("Metodo inserta");
			Opinion o=new Opinion();
//			o.setUsuario(2L);
			o.setUsuario(47L);
			o.setIdProduto(42L);
			o.setValoracion(5);
			o.setTexto("me flipa");
			o.setData(LocalDateTime.now());
			opinionDAO.inserta(o);
			
			logger.info("Metodo getOpinions");
			Produto produto=new Produto();
			produto.setId(42L);
			List<Opinion> opinions=opinionDAO.getOpinions(produto);
			logger.info(opinions.toString());
			
			logger.info("Metodo getValoracionMedia");
			Produto produto1=new Produto();
			produto1.setId(42L);
			int opinionMedia=opinionDAO.getValoracionMedia(produto1);
			logger.info("opinion media = "+opinionMedia);
			
		}catch (Exception e) {
			e.printStackTrace();
		}

	}

}
