package atendaservices.test.dao;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.logging.Logger;

import atendaservices.dao.PedidoDAO;
import atendaservices.dao.UsuarioDAO;
import atendaservices.dao.impl.PedidoDAOImpl;
import atendaservices.dao.impl.UsuarioDAOImpl;
import atendaservices.model.PedidoCriteria;
import atendaservices.model.Pedido;

public class PedidoDAOTest {

	private static final Logger logger = Logger.getAnonymousLogger();
	
	public static void main(String[] args) {
		PedidoDAO pedidoDAO = new PedidoDAOImpl();
		UsuarioDAO usuarioDAO = new UsuarioDAOImpl();
		
		try {
//			logger.info("metodo getPedidoPorId");
//			logger.info(pedidoDAO.getPedidoPorId(504).toString());
//			
//			logger.info("metodo getPedidosPeriodo");
//			PedidoCriteria criterio = new PedidoCriteria();
//			ArrayList<Pedido> pedidos = pedidoDAO.getPedidosPeriodo(criterio);
//			for(Pedido pedido:pedidos) {
//				logger.info(pedido.toString());
//			}
//			logger.info("metodo getPedidosPeriodo");
//			PedidoCriteria criterio = new PedidoCriteria();
//			criterio.setPechado(false);
//			criterio.setRecibido(false);
//			criterio.setUsuario(usuarioDAO.findById(7L));
//			criterio.setConDevolucions(true);
//			criterio.setAta(LocalDateTime.now().minusMonths(8));
//			criterio.setDende(LocalDateTime.now().minusMonths(8).minusDays(2));
//			ArrayList<Pedido> pedidos = pedidoDAO.getPedidosPeriodo(criterio);
//			for(Pedido pedido:pedidos) {
//				logger.info(pedido.toString());
//			}
//			
//			logger.info("Metodo getDevolucionsDe");
//			ArrayList<Pedido> pedidos2 = pedidoDAO.getDevolucionsDe(pedidoDAO.getPedidoPorId(504));
//			for(Pedido pedido:pedidos2) {
//				logger.info(pedidos2.toString());
//			}
//			
//			logger.info("Metodo inserta");
//			Pedido pedido=new Pedido();
//			pedido.setCliente(usuarioDAO.findById(2L));
//			pedido.setData(LocalDateTime.now());
//			pedido.setPechado(false);
//			pedido.setRecibido(false);
//			logger.info(pedidoDAO.getPedidoPorId(pedidoDAO.inserta(pedido)).toString());
//			
//			logger.info("Metodo actualiza");
//			Pedido pedido2=new Pedido();
//			pedido2.setId(526L);
//			pedido2.setPechado(true);
//			pedido2.setRecibido(true);
//			pedidoDAO.actualiza(pedido2);
//			logger.info("Actualizacion- " + pedidoDAO.getPedidoPorId(Integer.parseInt(pedido2.getId().toString())));
//			
//			logger.info("Metodo borra");
//			pedidoDAO.borra(pedido2);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
