package atendaservices.test.dao;

import java.util.ArrayList;
import java.util.logging.Logger;

import atendaservices.dao.Results;
import atendaservices.dao.UsuarioDAO;
import atendaservices.dao.impl.UsuarioDAOImpl;
import atendaservices.dao.util.ConnectionManager;
import atendaservices.model.Produto;
import atendaservices.model.Usuario;
import atendaservices.service.ProdutoCriteria;
public class UsuarioDAOTest {

	private static final Logger logger = Logger.getAnonymousLogger();
	
	
	public static void main(String[] args) {
		UsuarioDAO usuarioDAO=new UsuarioDAOImpl();
		
		try {
//			logger.info("Metodo FindById");
//			logger.info((usuarioDAO.findById(ConnectionManager.getConnection(), 1L)).toString());
//			
//			logger.info("Metodo FindByEmail-username");
//			logger.info((usuarioDAO.findByEmail(ConnectionManager.getConnection(), "fran@gmail.com")).toString());
//			
//			logger.info("Metodo FindAll");
//			ArrayList<Usuario> listaTodosUsuarios=usuarioDAO.findAll(ConnectionManager.getConnection());
//			for(Usuario usuario:listaTodosUsuarios) {
//				logger.info(usuario.toString());
//			}
//			
//			logger.info("Metodo Create");
//			Usuario u1=new Usuario();
//			u1.setUsername("probando1");
//			u1.setPassword("abc123.");
//			u1.setNome("probando1Nome");
//			u1.setRol("ADMIN");
//			u1.setAvatar("avatardeprobando1");
//			u1.setBaixa(false);
//			usuarioDAO.create(ConnectionManager.getConnection(), u1);
//			
//			logger.info("Metodo Update");
//			Usuario u2=new Usuario();
//			u2.setId(41L);
//			u2.setUsername("probando1editandto");
//			u2.setPassword("abc123.sdfsdf");
//			u2.setNome("EDITANDO probando221Nome");
//			u2.setRol("ADMIN");
//			u2.setAvatar("avatardeprobando1EDITANDO");
//			u2.setBaixa(false);
//			usuarioDAO.update(ConnectionManager.getConnection(), u2);
//			
			logger.info("Metodo SoftDelete");
			usuarioDAO.softDelete(ConnectionManager.getConnection(), 41L);
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

}
