package atendaservices.test.dao;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.logging.Logger;

import atendaservices.dao.LineaPedidoDAO;
import atendaservices.dao.PedidoDAO;
import atendaservices.dao.UsuarioDAO;
import atendaservices.dao.ProdutoDAO;
import atendaservices.model.LineaPedido;
import atendaservices.model.Pedido;
import atendaservices.dao.impl.LineaPedidoDAOImpl;
import atendaservices.dao.impl.PedidoDAOImpl;
import atendaservices.dao.impl.ProdutoDAOImpl;
import atendaservices.dao.impl.UsuarioDAOImpl;

public class LineaPedidoDAOTest {

	private static final Logger logger = Logger.getAnonymousLogger();
	
	public static void main(String[] args) {
		LineaPedidoDAO lineaPedidoDAO = new LineaPedidoDAOImpl();
		UsuarioDAO usuarioDAO = new UsuarioDAOImpl();
		PedidoDAO pedidoDAO = new PedidoDAOImpl();
		ProdutoDAO produtoDAO = new ProdutoDAOImpl();
		
		try {
//			logger.info("metodo inserta");
//				logger.info("Metodo inserta (Pedido)");
//				Pedido pedido=new Pedido();
//				pedido.setCliente(usuarioDAO.findById(2L));
//				pedido.setData(LocalDateTime.now());
//				pedido.setPechado(false);
//				pedido.setRecibido(false);
//				logger.info(pedidoDAO.getPedidoPorId(pedidoDAO.inserta(pedido)).toString());
//				
//			LineaPedido linea=new LineaPedido();
//			linea.setProduto(produtoDAO.findById(2L));
//			linea.setDesconto(10L);
//			linea.setUnidades(10L);
//			linea.setPrezo(10.9);
//			linea.setCoste(10.2);
//			
//			logger.info("linea- " 
//			+ lineaPedidoDAO.getById((long) lineaPedidoDAO.inserta(linea, pedidoDAO.getPedidoPorId(534))));
//			
//			
//			logger.info("metodo actualiza");
//			LineaPedido linea=new LineaPedido();
//			linea.setId(564L);
//			linea.setUnidades(55L);
//			lineaPedidoDAO.actualiza(linea);
//			
//			logger.info("metodo borra");
//			LineaPedido linea=new LineaPedido();
//			linea.setId(564L);
//			lineaPedidoDAO.borra(linea);
//			
//			logger.info("metodo getByPedido");
//			ArrayList<LineaPedido> lineas=(ArrayList<LineaPedido>) lineaPedidoDAO.getByPedido(504L);
//			for(LineaPedido line:lineas) {
//				logger.info(line.toString());
//			}
//			
//			logger.info("metodo getById");
//			logger.info(lineaPedidoDAO.getById(530L).toString());
//			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
