package atendaservices.test.services;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.logging.Logger;

import atendaservices.model.LineaPedido;
import atendaservices.model.Pedido;
import atendaservices.service.UsuarioService;
import atendaservices.service.PedidoService;
import atendaservices.service.ProdutoService;
import atendaservices.service.LineaPedidoService;
import atendaservices.service.impl.LineaPedidoServiceImpl;
import atendaservices.service.impl.PedidoServiceImpl;
import atendaservices.service.impl.ProdutoServiceImpl;
import atendaservices.service.impl.UsuarioServiceImpl;

public class LineaPedidoServiceTest {

	private static final Logger logger = Logger.getAnonymousLogger();
	public static void main(String[] args) {
		LineaPedidoService lineaPedidoService = new LineaPedidoServiceImpl();
		UsuarioService usuarioService = new UsuarioServiceImpl();
		PedidoService pedidoService = new PedidoServiceImpl();
		ProdutoService produtoService = new ProdutoServiceImpl();
		
		try {
//			logger.info("Metodo inserta");
//			logger.info("Metodo inserta (Pedido)");
//			Pedido pedido=new Pedido();
//			pedido.setCliente(usuarioService.findById(2L));
//			pedido.setData(LocalDateTime.now());
//			pedido.setPechado(false);
//			pedido.setRecibido(false);
//			pedidoService.inserta(pedido);
			
//			LineaPedido linea=new LineaPedido();
//			linea.setProduto(produtoService.findById(2L));
//			linea.setDesconto(10L);
//			linea.setUnidades(10L);
//			linea.setPrezo(10.9);
//			linea.setCoste(10.2);
//			logger.info("id de linea: " + lineaPedidoService.inserta(linea, pedidoService.getPedidoPorId(527)));
//			
//			logger.info("Metodo actualiza");
//			LineaPedido linea=new LineaPedido();
//			linea.setId(558L);
//			linea.setUnidades(55L);
//			lineaPedidoService.actualiza(linea);
//			
////			logger.info("Metodo getUnidadesDevoltasDe");
////			logger.info("unidades devoltas: " + lineaPedidoService.getUnidadesDevoltasDe());
//			
//			logger.info("Metodo borra");
//			LineaPedido linea=new LineaPedido();
//			linea.setId(558L);
//			lineaPedidoService.borra(linea);
//			
//			logger.info("Metodo getByPedido");
//			ArrayList<LineaPedido> lineas=(ArrayList<LineaPedido>) lineaPedidoService.getByPedido(505L);
//			for(LineaPedido line:lineas) {
//				logger.info(line.toString());
//			}
//			
//			logger.info("Metodo getById");
//			logger.info(lineaPedidoService.getById(531L).toString());
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}
