package atendaservices.test.services;

import java.util.ArrayList;
import java.util.logging.Logger;

import atendaservices.dao.Results;
import atendaservices.dao.util.ConnectionManager;
import atendaservices.model.Produto;
import atendaservices.model.ProdutoCriteria;
import atendaservices.service.ProdutoService;
import atendaservices.service.impl.ProdutoServiceImpl;

public class ProdutoServiceTest {

	private static final Logger logger = Logger.getAnonymousLogger();
	
	public static void main(String[] args) {
		ProdutoService produtoService=new ProdutoServiceImpl();

		try {
//			logger.info("Metodo FindById");
//			logger.info((produtoService.findById( 2L)).toString());
//			
//			logger.info("Metodo FindAll");
//			ArrayList<Produto> listaTodosProdutos=produtoService.findAll();
//			for(Produto produto:listaTodosProdutos) {
//				logger.info(produto.toString());
//			}
//			
//			logger.info("Metodo FindBy");
//			String prob="p";
//			ProdutoCriteria criterio=new ProdutoCriteria(null, null, null, null, prob);
//			Results<Produto> listaProdutos=produtoService.findBy(criterio, 0, 3);
//			logger.info(listaProdutos.getPage().toString());
//			
//			logger.info("Metodo Create");
//			Produto p1=new Produto();
//			p1.setIdCategoria(1);
//			p1.setIdMarca(1L);
//			p1.setNome("prodProbaService");
//			p1.setPrezo(2.2);
//			p1.setDesconto(3);
//			p1.setCoste(1.1);
//			p1.setIva(21);
//			p1.setStock(5L);
//			p1.setFoto("prodProbaSERVICE.jpg");
//			p1.setBaixa(false);
//			produtoService.create(p1);
//			
//			logger.info("Metodo Update");
//			Produto p2=new Produto();
//			p2.setId(47L);
//			//p2.setIdCategoria(2);
//			p2.setIdMarca(2L);
//			p2.setNome("prodProbaServiceModificado");
//			p2.setPrezo(10.0);
//			p2.setDesconto(10);
//			p2.setCoste(9.0);
//			p2.setIva(21);
//			p2.setStock(15L);
//			p2.setFoto("prodProbaServiceModificado90.jpg");
//			p2.setBaixa(false);
//			logger.info(produtoService.update(p2).toString());
//		
//			logger.info("Metodo asignarProdutoCategoria");
//			logger.info("asignar= "+produtoService.asignarProdutoCategoria(47L, 1));
//			
//			logger.info("Metodo asignarProdutoMarca");
//			logger.info("asignar= "+produtoService.asignarProdutoMarca(38L, 2L));
//			
//			logger.info("Metodo SoftDelete");
//			produtoService.softDelete(47L);
//			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}
