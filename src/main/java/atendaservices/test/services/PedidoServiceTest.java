package atendaservices.test.services;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.logging.Logger;

import atendaservices.dao.UsuarioDAO;
import atendaservices.dao.impl.UsuarioDAOImpl;
import atendaservices.model.Pedido;
import atendaservices.model.PedidoCriteria;
import atendaservices.service.PedidoService;
import atendaservices.service.UsuarioService;
import atendaservices.service.impl.PedidoServiceImpl;
import atendaservices.service.impl.UsuarioServiceImpl;

public class PedidoServiceTest {

	private static final Logger logger = Logger.getAnonymousLogger();
	
	public static void main(String[] args) {
		PedidoService pedidoService=new PedidoServiceImpl();
		UsuarioService usuarioService = new UsuarioServiceImpl();
		
		try {
//			logger.info("Metodo getPedidoPorId");
//			logger.info(pedidoService.getPedidoPorId(504).toString());
//			
//			logger.info("metodo getPedidosPeriodo");
//			PedidoCriteria criterio = new PedidoCriteria();
//			criterio.setPechado(false);
//			criterio.setRecibido(false);
//			criterio.setUsuario(usuarioService.findById(7L));
//			criterio.setConDevolucions(true);
//			criterio.setAta(LocalDateTime.now().minusMonths(8));
//			criterio.setDende(LocalDateTime.now().minusMonths(8).minusDays(2));
//			ArrayList<Pedido> pedidos = pedidoService.getPedidosPeriodo(criterio);
//			for(Pedido pedido:pedidos) {
//				logger.info(pedido.toString());
//			}
//			
//			logger.info("Metodo getDevolucionsDe");
//			Pedido ped = new Pedido();
//			ped.setId(505L);
//			ArrayList<Pedido> pedidos = pedidoService.getDevolucionsDe(ped);
//			for(Pedido pedid:pedidos) {
//				logger.info(pedid.toString());
//			}
//			
//			logger.info("Metodo inserta");
//			Pedido pedido=new Pedido();
//			pedido.setCliente(usuarioService.findById(1L));
//			pedido.setData(LocalDateTime.now());
//			pedido.setPechado(false);
//			pedido.setRecibido(false);
//			logger.info("id pedido= " + pedidoService.inserta(pedido));
//			
//			logger.info("Metodo actualiza");
//			Pedido pedido=new Pedido();
//			pedido.setId(527L);
//			pedido.setPechado(true);
//			pedido.setRecibido(true);
//			pedidoService.actualiza(pedido);
//			
//			logger.info("Metodo borra");
//			Pedido pedido=new Pedido();
//			pedido.setId(525L);
//			pedidoService.borra(pedido);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
