package atendaservices.test.services;

import atendaservices.service.UsuarioService;
import atendaservices.service.impl.UsuarioServiceImpl;

import java.util.ArrayList;
import java.util.logging.Logger;

import org.mindrot.jbcrypt.BCrypt;

import atendaservices.dao.util.ConnectionManager;
import atendaservices.model.Usuario;

public class UsuarioServiceTest {

	private static final Logger logger = Logger.getAnonymousLogger();
	
	public static void main(String[] args) {
		UsuarioService usuarioService=new UsuarioServiceImpl();
		
		try {
//			logger.info("Metodo login");
//			logger.info((usuarioService.login("admin@gmail.com", "abc123.")).toString());
//			
//			logger.info("Metodo FindById");
//			logger.info((usuarioService.findById(2L)).toString());
//			
//			logger.info("Metodo FindByEmail(username");
//			logger.info((usuarioService.findByEmail("fran@gmail.com")).toString());
//			
//			logger.info("Metodo FindAll");
//			ArrayList<Usuario> listaTodosUsuarios=usuarioService.findAll();
//			for(Usuario usuario:listaTodosUsuarios) {
//				logger.info(usuario.toString());
//			}
			
//			logger.info("Metodo Create");
//			Usuario u1=new Usuario();
//			u1.setUsername("probandoServiceUser");
//			u1.setPassword(BCrypt.hashpw("abc123.", BCrypt.gensalt(12)));
//			u1.setNome("probaServiceUser");
//			u1.setRol("BASIC");
//			u1.setAvatar("avatardeprobandoserviceuser89.jpg");
//			u1.setBaixa(false);
//			usuarioService.register(u1);
//			
//			logger.info("Metodo Update");
//			Usuario u2=new Usuario();
//			u2.setId(46L);
//			u2.setUsername("probandoServiceModificado");
//			u2.setPassword("abc123.");
//			u2.setNome("probandoNomeMod");
//			u2.setRol("BASIC");
//			u2.setAvatar("probandoServiceModificado56.jpg");
//			u2.setBaixa(true);
//			usuarioService.update(u2);
//			
//			logger.info("Metodo SoftDelete");
//			usuarioService.softDelete(45L);
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

}
