package resource;

import java.security.Key;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.logging.Logger;

import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import atendaservices.model.Usuario;
import atendaservices.service.UsuarioService;
import atendaservices.service.impl.UsuarioServiceImpl;
import util.KeyGenerator;
import util.SimpleKeyGenerator;
import io.jsonwebtoken.Jwts;

@Path("/")
public class AccesoResource {
   private static final Logger logger = Logger.getLogger(AccesoResource.class.getName())  ;
   private UsuarioService usuarioService = new UsuarioServiceImpl();
   private  KeyGenerator keyGenerator = new SimpleKeyGenerator();
	
	
	@Path("auth")
	@POST 
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response authUsuario(@Context UriInfo uriInfo, Usuario usuario) {
		// se evia user como json slo co los campos de username y password
		Usuario user=null;
		String token = null;
		try {
			user = usuarioService.login(usuario.getUsername(), usuario.getPassword());
			try {
				token = issueToken(uriInfo, usuario.getUsername());
				logger.info("token xerado: " + token);
			} catch (Exception e) {
				logger.info(e.getMessage());
				e.printStackTrace();
			}
		} catch (Exception e) {
			logger.info("Error: Las contraseñas no coinciden,o usuario no existe, o algo asi");
		}
		if (user!=null) {
			logger.info("token xerado: " + token);
			return Response.ok().header("Access-Control-Allow-Origin", "*").header("Authorization", "Bearer " + token)
					.build();

		} else {
			JsonObject json = Json.createObjectBuilder().add("mensaxe", "credenciais incorrectas").build();
			return Response.status(Response.Status.UNAUTHORIZED).entity(json).build();
		}
		
		
		
	}
	private String issueToken(UriInfo uriInfo, String username) {
		
		Key hmackey = keyGenerator.generateKey();
		
		String jwtToken = Jwts.builder()
				.setSubject(username)
				.setIssuer(uriInfo.getAbsolutePath().toString())
				.setIssuedAt(new Date()).setExpiration(Timestamp.valueOf(LocalDateTime.now().plusMinutes(2L)))
				.signWith(hmackey)
				.compact();
				return jwtToken; 	   	 	
	}	
    @Path("register")
    @POST 
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response registerUsuario(Usuario usuario) {		
		System.out.println("intento registrar usuario: "+usuario.getUsername());
		try {
			//usuario.setRol("BASIC");
			usuarioService.register(usuario);
			return Response.ok(usuario).status(Response.Status.CREATED).build();
		}catch (Exception e) {
			logger.info(e.getMessage());
		}
		 return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();		
	}
}
