package resource;

import java.io.IOException;
import java.security.Key;
import java.util.logging.Logger;

import javax.annotation.Priority;
import javax.inject.Inject;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

import atendaservices.model.Usuario;
import atendaservices.service.UsuarioService;
import atendaservices.service.impl.UsuarioServiceImpl;

import javax.ws.rs.Priorities;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import util.KeyGenerator;
import util.SimpleKeyGenerator;

@JWTTokenNeeded
@Provider
@Priority(Priorities.AUTHENTICATION)
public class JWTTokenNeededFilter implements ContainerRequestFilter {
	private static final Logger logger = Logger.getAnonymousLogger();
	private UsuarioService usuarioService = new UsuarioServiceImpl();
	@Inject
	private KeyGenerator keyGenerator = new SimpleKeyGenerator();
	
	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		try {
			// Get the HTTP Authorization header from the request
			String authorizationHeader = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);
			// Extract the token from the HTTP Authorization header
			// lanza exception si no trae cabecera Authorization bearer
			String token = authorizationHeader.substring("Bearer".length()).trim();			
			// validate the token (hmackey)		
			Key hmackey = keyGenerator.generateKey();
			Jws<Claims> jwt = Jwts.parserBuilder()
					.setSigningKey(hmackey)
					.build()
					.parseClaimsJws(token);		
			logger.info(jwt.toString());   
			String email = jwt.getBody().getSubject();
			logger.info(email);
			Usuario principal = usuarioService.findByEmail(email);
			logger.info(principal.toString());
			// construirmos e inxectamos (set) SecurityContext para que @RolesAllowed,
			// isUserInRole(), getUserPrincipal() funcionen
			JWTSecurityContext ctx = new JWTSecurityContext(principal, requestContext.getSecurityContext().isSecure());
			logger.info(ctx.toString());
			requestContext.setSecurityContext(ctx);
			///// PARA DEPURACIÓN /////
			logger.info("jwt: " + jwt);
			logger.info("principal: " + principal);
			logger.info("ctx: " + ctx);
		} catch (Exception e) {
			logger.info("Error en JTokenNeededFilter, SIN AUTORIZACION");
			requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
		}
	}
}