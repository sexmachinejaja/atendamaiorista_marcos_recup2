package resource;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import atendaservices.dao.Results;
import atendaservices.exceptions.DataException;
import atendaservices.model.Produto;
import atendaservices.model.ProdutoCriteria;
import atendaservices.service.ProdutoService;
import atendaservices.service.impl.ProdutoServiceImpl;

//@JWTTokenNeeded
@Path("/produtos")
//@Path("secured/produtos")
public class ProdutoResource {
	private ProdutoService produtoService = new ProdutoServiceImpl();
	private static final Logger logger = Logger.getAnonymousLogger();
		@GET
		@JWTTokenNeeded
	    @RolesAllowed({"BASIC","ADMIN"})
	    @Produces(MediaType.APPLICATION_JSON)
	    public Response getProdutos() {
			
			List < Produto > produtos=null;
	       try {
	    	  produtos = produtoService.findAll();
		        
			} catch (DataException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	       if (!produtos.isEmpty()) {
	            return Response.ok(produtos).build();
	        } else {
	            logger.info("produtos esta vacio");
	        	return Response.status(Response.Status.NOT_FOUND).build();
	        }
	    }
	 
	    @Path("/{idProduto}")
	    @GET
	    @JWTTokenNeeded
	    @RolesAllowed({"BASIC", "ADMIN"})
	    @Produces(MediaType.APPLICATION_JSON)
	    public Response getProdutoById(@PathParam("idProduto") Long idProduto) {
	       try {
	    	   Produto produto = produtoService.findById(idProduto);
		        if (produto != null) {
		            return Response.ok(produto).build();
		        } else {
		            return Response.status(Response.Status.NOT_FOUND).build();
		        }   
			} catch (DataException e) {
				return Response.status(Response.Status.NOT_FOUND).build();
			}
	    }
	    
	    @POST
	    @JWTTokenNeeded
	    @RolesAllowed({"ADMIN"})
	    @Produces(MediaType.APPLICATION_JSON)
	    @Consumes(MediaType.APPLICATION_JSON)
	    public Response createProduto(Produto produto) { 	
	    	Produto prod;
	    	try {
	    		Long idCreado=produtoService.create(produto);
	    		prod=produtoService.findById(idCreado);
	                // devolvemos o obxecto creado (serializado a JSON por RESTEasy)
	    		
	    	}catch (Exception e) {
	    		return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
	    	}
	    	return Response.ok(prod).status(Response.Status.CREATED).build();
	    }
	    
	    @PUT
	    @JWTTokenNeeded
	    @RolesAllowed({"ADMIN"})
	    @Path("/{idProduto}")
	    @Consumes(MediaType.APPLICATION_JSON)
	    public Response updateProduto(@PathParam("idProduto") Long idProduto, Produto produto) {
	    	Produto prod;
	    	produto.setId(idProduto);
	    	try {
	    		prod=produtoService.update(produto);
	    	}catch (Exception e) {
	    		return (Response.status(Response.Status.NOT_MODIFIED)).build();
	    	}	
	    	return Response.ok().build();
//	    	return Response.ok(prod).build();
	    }
	    
	    @Path("/{idProduto}")
	    @DELETE
	    @JWTTokenNeeded
	    @RolesAllowed({"ADMIN"})
	    @Produces(MediaType.APPLICATION_JSON)
	    public Response deleteProduto(@PathParam("idProduto") Long idProduto) {
	        try {
	        	boolean result = produtoService.softDelete(idProduto);
	 	        if (result) {
	 	            return Response.status(Response.Status.OK).build();
	 	        } else {
	 	        	return Response.status(Response.Status.NOT_FOUND).build();
	 	        }
			} catch (DataException e) {
				return Response.status(Response.Status.NOT_FOUND).build();
			}

	    }
	    
	    
	    @PUT
	    @Path("/{idProduto}/asignarmarca/{idMarca}")
//		@RolesAllowed("ADMIN")
		@Produces(MediaType.APPLICATION_JSON)
	    public Response updateProdutoMarca(@PathParam("idProduto") long idProduto, @PathParam("idMarca") long idMarca) {
	    	Boolean asignado;
	    	try {
				asignado = produtoService.asignarProdutoMarca(idProduto, idMarca);
				if(asignado) {
					Produto produto=produtoService.findById(idProduto);
					return Response.ok(produto).status(Response.Status.ACCEPTED).build();
				}else {
					return Response.status(Response.Status.NOT_MODIFIED).build();
				}
	    	} catch (DataException e) {
	    		return Response.status(Response.Status.NOT_MODIFIED).build();
			}
	    }
	    
	    @PUT
	    @Path("/{idProduto}/asignarcategoria/{idCat}")
//		@RolesAllowed("ADMIN")
		@Produces(MediaType.APPLICATION_JSON)
	    public Response updateProdutoCategoria(@PathParam("idProduto") long idProduto, @PathParam("idCat") int idCategoria) {
	    	Boolean asignado;
	    	try {
				asignado = produtoService.asignarProdutoCategoria(idProduto, idCategoria);
				if(asignado) {
					Produto produto=produtoService.findById(idProduto);
					return Response.ok(produto).status(Response.Status.ACCEPTED).build();
				}else {
					return Response.status(Response.Status.NOT_MODIFIED).build();
				}
	    	} catch (DataException e) {
	    		return Response.status(Response.Status.NOT_MODIFIED).build();
			}
	    }
	    
	    @GET
		@Path("/search/{patron}/{startIndex}")
//		@RolesAllowed({"ADMIN","BASIC"})
	    @Produces(MediaType.APPLICATION_JSON)
	    public Response getUsersPatron(@PathParam("patron") String patron, @PathParam("startIndex") int startIndex) {
			ProdutoCriteria criteria = new ProdutoCriteria();
			criteria.setNome(patron);
//			criteria.setIdMarca(patron);
			Results<Produto> produtos = null;
			try {
				produtos = produtoService.findBy(criteria, startIndex, 3);
			} catch (DataException e) {
				e.printStackTrace();
			}

	        if (!produtos.getPage().isEmpty()) {
	            return Response.ok(produtos.getPage()).build();
	        } else {
	            return Response.status(Response.Status.NOT_FOUND).build();
	        }
	    }
}
