package resource;

import java.util.List;
import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import atendaservices.exceptions.DataException;
import atendaservices.model.Usuario;
import atendaservices.service.exceptions.UserNotFoundException;
import atendaservices.service.UsuarioService;
import atendaservices.service.impl.UsuarioServiceImpl;

@Path("/usuarios")
public class UsuarioResource {
	private UsuarioService usuarioService = new UsuarioServiceImpl();
	private static final Logger logger = Logger.getAnonymousLogger();
	
	
	
	@GET
	//@RolesAllowed({"BASIC", "ADMIN"})
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUsuarios() {
		
		List < Usuario > usuarios=null;
       try {
    	   usuarios = usuarioService.findAll();
	        
		} catch (DataException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       if (!usuarios.isEmpty()) {
            return Response.ok(usuarios).build();
        } else {
            logger.info("usuarios esta vacio");
        	return Response.status(Response.Status.NOT_FOUND).build();
        }
    }
 
    @Path("/{idUsuario}")
    @GET
    //@RolesAllowed({"BASIC", "ADMIN"})
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUsuarioById(@PathParam("idUsuario") Long idUsuario) {
    	Usuario usuario;
    	try {
    	   usuario = usuarioService.findById(idUsuario);
		} catch (DataException | UserNotFoundException e) {
			return Response.status(Response.Status.NOT_FOUND).build();
		}
       return Response.ok(usuario).build();
    }
    
    @POST
    //@RolesAllowed({"ADMIN"})
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createUsuario(Usuario usuario) { 	
    	Usuario user;
    	try {
    		user=usuarioService.register(usuario);
    	}catch (Exception e) {
    		return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
    	}
    	return Response.ok(user).status(Response.Status.CREATED).build();
    }
    
    @PUT
    //@RolesAllowed({"ADMIN"})
    @Path("/{idUsuario}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateUsuario(@PathParam("idUsuario") Long idUsuario, Usuario usuario) {
    	Usuario user;
    	usuario.setId(idUsuario);
    	try {
    		user=usuarioService.update(usuario);
    	}catch (Exception e) {
    		return (Response.status(Response.Status.NOT_MODIFIED)).build();
    	}	
    	return Response.ok().build();
//    	return Response.ok(user).build();
    }
    
    @Path("/{idUsuario}")
    @DELETE
    //@RolesAllowed({"ADMIN"})
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteUsuario(@PathParam("idUsuario") Long idUsuario) {
        try {
        	usuarioService.softDelete(idUsuario);
 	        return Response.status(Response.Status.OK).build();
		} catch (DataException e) {
			return Response.status(Response.Status.NOT_FOUND).build();
		}
    }
}
