package resource;

import java.security.Principal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.logging.Logger;

import javax.ws.rs.core.SecurityContext;

import atendaservices.model.Usuario;

public class JWTSecurityContext implements SecurityContext {

	private static final Logger logger = Logger.getLogger(JWTSecurityContext.class.getName());

	private Usuario principal; // user xa conten os roles....
	private boolean isSecure;
	private String rol;

	public JWTSecurityContext(Usuario principal, boolean isSecure) {
		
		this.principal = principal;
		this.isSecure = isSecure;
		this.rol = principal.getRol();

	}

	@Override
	public Principal getUserPrincipal() {
		return (Principal) principal;
	}

	@Override
	public boolean isUserInRole(String role) {

		if(this.rol.equals(role)) {
			
			logger.info("securityContext: USER IS IN ROLE");
			return true;
		}else {
			
			logger.info("securityContext: USER IS NOT IN ROLE");
			return false;
		}
		
		// return roles.contains(role);
	}

	@Override
	public boolean isSecure() {
		return isSecure;
	}

	@Override
	public String getAuthenticationScheme() {
		StringBuilder builder = new StringBuilder();
		builder.append("JWTSecurityContext {").append("principal:").append(principal).append(",").append("roles:")
				.append(rol).append(",").append("isSecure:").append(isSecure).append("}");
		return builder.toString();
	}
}
